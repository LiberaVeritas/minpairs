module Main exposing (..)

import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input

import Browser
import Html exposing (Html, audio)
import Html.Attributes exposing (controls, src, style)
import Http

import Json.Decode exposing (Decoder, index, string, field, map2, list, decodeString, errorToString)
import Random
import Random.List as Random

import Tuple exposing (first, second)
import Set exposing (Set)
import Dict exposing (Dict)


baseUrl =
    "https://liberaveritas.gitlab.io/minpairs/"

testSetsUrl =
    baseUrl ++ "test-sets.json"

audioListsUrl =
    baseUrl ++ "audio-lists.json"

audioDir =
    baseUrl ++ "words/"



-- TestSet and Json


type IpaPair
    = IpaPair ( String, String )

type WordPair
    = WordPair ( String, String )

type alias TestSet =
    { ipaGroupPair : IpaPair
    , wordPairs : List WordPair
    }

type alias TestSetDecode =
    { ipaGroupPair : ( String, String )
    , wordPairs : List ( String, String )
    }


testSetDecoder : Decoder TestSetDecode
testSetDecoder =
    map2 TestSetDecode
        (field "ipaGroupPair" <|
            map2 Tuple.pair (index 0 string) (index 1 string))
        (field "wordPairs" <|
            list <| map2 Tuple.pair (index 0 string) (index 1 string))

    
    
    
-- Main


main : Program () Model Msg
main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = \_ -> Sub.none
        }

            
            
-- Model


type Model
    = Error String
    | Ready State
    | Partial PartialState


initialState : PartialState
initialState =
        PartialState [] Nothing Nothing Nothing Nothing Nothing [] empty


init : () -> (Model, Cmd Msg)
init _ =
    ( Partial initialState
    , Http.get
        { url = testSetsUrl
        , expect = Http.expectJson handleTestSetListResult (list testSetDecoder)
        }
    )


type alias PartialState =
    { testSets : List TestSet
    , testSet : Maybe TestSet
    , testWordPair : Maybe WordPair
    , audioListPair : Maybe ( List String, List String )
    , wordAudioPair : Maybe ( WordAudio, WordAudio )
    , testWordAudio : Maybe WordAudio
    , allTestSets : List TestSet
    , selectedSet : IpaPairSet
    }


type alias State =
    { testSets : List TestSet
    , testSet : TestSet
    , testWordPair : WordPair
    , audioListPair : ( List String, List String )
    , wordAudioPair : ( WordAudio, WordAudio )
    , testWordAudio : WordAudio
    , guess : Guess
    , pairsToggle : PairsToggle
    , selectedSet : IpaPairSet
    , allTestSets : List TestSet
    }

type Guess
    = Left
    | Right
    | None

type PairsToggle
    = Hidden
    | Shown


fulfillState : PartialState -> Model
fulfillState ({ testSets, testSet, testWordPair, audioListPair, wordAudioPair, testWordAudio } as part) =
    case ( testSets, testSet, testWordPair ) of
        ( _::_, Just ts, Just (WordPair twp) ) ->
            case ( audioListPair, wordAudioPair, testWordAudio ) of
                ( Just alp, Just wap, Just twa ) ->
                    Ready
                        { testSets = testSets
                        , testSet = ts
                        , testWordPair = WordPair twp
                        , audioListPair = alp
                        , wordAudioPair = wap
                        , testWordAudio = twa
                        , guess = None
                        , pairsToggle = Hidden
                        , selectedSet = part.selectedSet
                        , allTestSets = part.allTestSets
                        }
                _ ->
                    Error "Failed to fulfill partial state, stage 2"
        ( [], _, _ ) ->
            Error "Empty initial test set list"
            
        _ ->
            Error "Failed to fulfill partial state, stage 1"

            
type alias WordAudio =
    { wordIpa : String
    , audio : String
    }





-- update

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case ( model, msg ) of
        ( _, ErrorMsg s ) ->
            ( Error s
            , Cmd.none
            )
            
        ( Ready state, RepeatTest ) ->
            ( Partial
                { initialState
                    | allTestSets = state.allTestSets
                    , testSets = state.testSets
                    , testSet = Just state.testSet
                    , selectedSet = state.selectedSet
                    , testWordPair = Just state.testWordPair
                    , audioListPair = Just state.audioListPair
                    }
            , Random.generate (handleAudioPairResult (Just state.testWordPair)) <|
                Random.pair
                    (Random.choose (first state.audioListPair))
                    (Random.choose (second state.audioListPair))
            )
        
        ( Ready state, GuessedLeft ) ->
            ( Ready { state | guess = Left }
            , Cmd.none
            )
        
        ( Ready state, GuessedRight ) ->
            ( Ready { state | guess = Right }
            , Cmd.none
            )
        
        ( Ready state, TogglePairs ) ->
            case state.pairsToggle of
                Hidden ->
                    ( Ready { state | pairsToggle = Shown}
                    , Cmd.none
                    )
                
                Shown ->
                    ( Ready { state | pairsToggle = Hidden }
                    , Cmd.none
                    )
            
        ( Partial part, TestSetListLoaded lst ) ->
            if (List.isEmpty lst) then
                ( Error "Empty initial test set list when building state"
                , Cmd.none
                )
                
            else
                ( Partial { part | allTestSets = lst, testSets = lst }
                , Random.generate handleTestSetResult ( Random.choose lst )
                )
        
        ( Partial part, TestSetChosen ts ) ->
            ( Partial { part | testSet = Just ts }
            , Random.generate handleTestWordPairResult ( Random.choose ts.wordPairs )
            )
        
        ( Partial part, TestWordPairChosen (WordPair wp) ) ->
            ( Partial { part | testWordPair = Just (WordPair wp) }
            , Http.get
                { url = audioListsUrl
                , expect = Http.expectJson (handleAudioListPairResult (WordPair wp)) <|
                    map2 Tuple.pair
                        (field (first wp) (list string))
                        (field (second wp) (list string))
                }
            )
        
        ( Partial part, GotAudioListPair ( lst1, lst2 ) ) ->
            ( Partial { part | audioListPair = Just ( lst1, lst2 ) }
            , Random.generate (handleAudioPairResult part.testWordPair) <|
                Random.pair
                    (Random.choose lst1)
                    (Random.choose lst2)
            )
            
        ( Partial part, WordAudioPairChosen wordAudioPair ) ->
            ( Partial { part | wordAudioPair = Just wordAudioPair }
            , Random.generate (handleWordAudioResult wordAudioPair) (Random.uniform first [ first, second ])
            )
        
        ( Partial part, WordAudioChosen wordAudio ) ->
            ( fulfillState { part | testWordAudio = Just wordAudio }
            , Cmd.none
            )
        
        ( Error e, _ ) ->
            ( Error e
            , Cmd.none
            )
        
        ( Ready { allTestSets, testSets, selectedSet }, NextTest ) ->
            ( Partial
                { initialState
                    | allTestSets = allTestSets
                    , testSets = testSets
                    , selectedSet = selectedSet
                    }
            , Random.generate handleTestSetResult ( Random.choose testSets )
            )
        
        ( Ready state, ToggleSelected ts bool ) ->
            let
                new =
                    if bool then
                        add ts.ipaGroupPair state.selectedSet
                    
                    else
                        remove ts.ipaGroupPair state.selectedSet
            in
            ( Ready { state | selectedSet = new }
            , Cmd.none
            )
        
        ( Ready ({ allTestSets, selectedSet } as state), TestSelectedPairs ) ->
            if (isEmpty selectedSet) then
                ( Ready state, Cmd.none )
                
            else
            let
                testSets =
                    List.filter
                        (\ts -> member ts.ipaGroupPair selectedSet)
                        allTestSets
            
            in
            ( Partial
                { initialState
                    | allTestSets = allTestSets
                    , testSets = testSets
                    , selectedSet = selectedSet
                    }
            , Random.generate handleTestSetResult ( Random.choose testSets )
            )
    
        ( Ready state, _ ) ->
            ( Error "Improper message while state was ready"
            , Cmd.none
            )
        
        ( Partial state, _ ) ->
            ( Error "Improper message while state was partial"
            , Cmd.none
            )

        


-- Msg

type Msg
    = GuessedLeft
    | GuessedRight
    | RepeatTest
    | NextTest
    | TestSetListLoaded (List TestSet)
    | TestSetChosen TestSet
    | TestWordPairChosen WordPair
    | GotAudioListPair ( List String, List String )
    | WordAudioPairChosen ( WordAudio, WordAudio )
    | WordAudioChosen WordAudio
    | TogglePairs
    | TestSelectedPairs
    | ToggleSelected TestSet Bool
    | ErrorMsg String


handleHttpError : String -> Http.Error -> String
handleHttpError src error =
    case error of
        Http.BadUrl s ->
            "Bad url: " ++ s
        
        Http.Timeout ->
            "Http Timeout"
        
        Http.NetworkError ->
            "Http Network Error"
        
        Http.BadStatus int ->
            "Http Error: " ++ String.fromInt int ++ ". Requested: " ++ src
        
        Http.BadBody s ->
            s



handleTestSetListResult : Result Http.Error (List TestSetDecode) -> Msg
handleTestSetListResult res =
    case res of
        Ok [] ->
            ErrorMsg "Got empty test set list attempting to decode Json"
            
        Ok lst ->
            TestSetListLoaded <|
                List.map
                    (\tsd -> { ipaGroupPair = IpaPair tsd.ipaGroupPair
                            , wordPairs = List.map (\pair -> WordPair pair) tsd.wordPairs
                            }
                    )
                    lst
            
        Err e ->
            ErrorMsg <| handleHttpError testSetsUrl e


handleTestSetResult : (Maybe TestSet, List TestSet) -> Msg
handleTestSetResult ( res, _ ) =
    case res of
        Nothing ->
            ErrorMsg "Failed to select a test set"
        
        Just testSet ->
            TestSetChosen testSet


handleTestWordPairResult : ( Maybe WordPair, List WordPair ) -> Msg
handleTestWordPairResult ( res, _ ) =
    case res of
        Nothing ->
            ErrorMsg "Failed to select a test word pair"
        
        Just (WordPair wp) ->
            TestWordPairChosen (WordPair wp)



handleAudioListPairResult : WordPair -> Result Http.Error (List String, List String) -> Msg
handleAudioListPairResult (WordPair ( w1, w2 )) res =
    case res of
        Err e ->
            ErrorMsg <| handleHttpError audioListsUrl e
        
        Ok ( [], _ ) ->
            ErrorMsg <| "Got empty audio list for " ++ w1
            
        Ok ( _, [] ) ->
            ErrorMsg <| "Got empty audio list for " ++ w2
                        
        Ok pair ->
            GotAudioListPair pair


handleAudioPairResult : Maybe WordPair -> ( (Maybe String, List String), (Maybe String, List String) ) -> Msg
handleAudioPairResult wordPair ( res1, res2 ) =
    case ( wordPair, res1, res2 ) of
        ( Just (WordPair wp), ( Just audio1, _ ), ( Just audio2, _ ) ) ->
            WordAudioPairChosen
                ( { wordIpa = (first wp), audio = audio1 }
                , { wordIpa = (second wp), audio = audio2 }
                )
        
        _ ->
            ErrorMsg "Failed to select word audio pair"


handleWordAudioResult : ( WordAudio, WordAudio ) -> (( WordAudio, WordAudio ) -> WordAudio) -> Msg
handleWordAudioResult tup fn =
    WordAudioChosen <| fn tup

    
    
    
    
            
-- view
            
      
      
view : Model -> Html Msg
view model =
    case model of
        Error e ->
            layout [] <| paragraph [] [ text e ]
        
        Partial _ ->
            layout [] <| text "Loading"
            
        Ready state ->
            layout [] <|
                column [ width fill, padding 50, spacing 50 ]
                    [ header
                    , mainPanel state
                    , pairsPanel state
                    ]



pairsPanel : State -> Element Msg
pairsPanel { allTestSets, pairsToggle, selectedSet } =
    case pairsToggle of
        Hidden ->
            column [ centerX, paddingXY 0 100 ] <|
                [ Input.button [ centerX, padding 10, Border.width 1, Border.rounded 6 ]
                    { onPress = Just TogglePairs, label = text "Choose pairs" }
                ]
            
        Shown ->
            column [ centerX, paddingXY 0 100, spacing 10 ] <|
                [ Input.button [ centerX, padding 10, Border.width 1, Border.rounded 6 ]
                    { onPress = Just TogglePairs, label = text "Hide pairs" }
                , column [] <| List.map (panelItem selectedSet) allTestSets
                , Input.button [ Border.width 1, Border.rounded 6, padding 10 ]
                    { onPress = Just TestSelectedPairs, label = text "Test chosen" }
                ]
                    

panelItem : IpaPairSet -> TestSet -> Element Msg
panelItem set ts =
    Input.checkbox []
        { onChange = ToggleSelected ts
        , icon = Input.defaultCheckbox
        , checked = member ts.ipaGroupPair set
        , label =
            Input.labelRight [ padding 5, spacing 10 ]
                (text <| ipaGroupPairString ts)
        }



mainPanel : State -> Element Msg
mainPanel state =
    case state.guess of
        None ->
            column [ centerX ]
                [ testColumn (ipaGroupPairString state.testSet) (audioFileString state.testWordAudio)
                , choiceRow state
                ]
        _ ->
            column [ centerX ]
                [ testColumn (ipaGroupPairString state.testSet) (audioFileString state.testWordAudio)
                , choiceRow state
                , row [ width <| px 200, padding 10 ]
                    [ Input.button [ alignLeft, Border.width 2, Border.rounded 6, padding 10]
                        { onPress = Just RepeatTest, label = el [ centerX ] <| text "Again" }
                    , Input.button [ alignRight, Border.width 2, Border.rounded 6, padding 10]
                        { onPress = Just NextTest, label = el [ centerX ] <| text "Next" }
                    ]
                ]


header : Element Msg
header =
    row [ width fill, padding 20, Border.width 2 ]
        [ el [ centerX ] <| text "French Minimal Pairs"
        ]


testColumn : String -> String -> Element Msg
testColumn ipa audioSource =
    column [ width fill, spacing 20 ]
    [ el [ centerX ] <| text ipa
    , el [ centerX ] <| html <|
            audio [ controls True, src audioSource, (Html.Attributes.style "width" "200px"), (Html.Attributes.type_ "audio/mp4") ] []
    ]

choiceRow : State -> Element Msg
choiceRow { guess, wordAudioPair, testWordAudio } =
    let
        ( wa1, wa2 ) = wordAudioPair
        twa = testWordAudio
    in
    row [ centerX, paddingXY 0 20, width fill, spacing 10 ]
        [ leftWordBox ( guess, wa1, twa )
        , rightWordBox ( guess, wa2, twa )
        ]


leftWordBox : ( Guess, WordAudio, WordAudio ) -> Element Msg
leftWordBox ( guess, wa, twa ) =
        case guess of
            Left ->
                column [ width (fillPortion 1), alignLeft, spacing 20 ]
                    [ Input.button
                        [ centerX, Border.width 2, Border.rounded 6
                        , padding 10, Background.color <|
                            if wa.wordIpa == twa.wordIpa then
                                color.green
                            else
                                color.red
                        ]
                        { onPress = Nothing, label = buttonLabel wa }
                    , el [ moveRight 25 ] <| html <|
                        audio
                            [ controls True, src (audioFileString wa)
                            ,style "width" "100px"
                            , (Html.Attributes.type_ "audio/mp4")
                            ]
                            []
                    ]
            
            None ->
                column [ width (fillPortion 1), alignLeft, spacing 20 ]
                    [ Input.button
                        [ centerX, Border.width 2, Border.rounded 6, padding 10 ]
                        { onPress = Just GuessedLeft, label = buttonLabel wa }
                    ]
            
            Right ->
                column [ width (fillPortion 1), alignLeft, spacing 20 ]
                    [ Input.button
                        [ centerX, Border.width 2, Border.rounded 6, padding 10 ]
                        { onPress = Nothing, label = buttonLabel wa }
                    , el [ moveRight 25 ] <| html <|
                        audio
                            [ controls True, src (audioFileString wa)
                            , style "width" "100px"
                            , (Html.Attributes.type_ "audio/mp4")
                            ]
                            []
                    ]
                

rightWordBox : ( Guess, WordAudio, WordAudio ) -> Element Msg
rightWordBox ( guess, wa, twa ) =
        case guess of
            Right ->
                column [ width (fillPortion 1), alignRight, spacing 20 ]
                    [ Input.button
                        [ centerX, Border.width 2, Border.rounded 6
                        , padding 10, Background.color <|
                            if wa.wordIpa == twa.wordIpa then
                                color.green
                            else
                                color.red
                        ]
                        { onPress = Nothing, label = buttonLabel wa }
                    , el [ moveRight 25 ] <| html <|
                        audio
                            [ controls True, src (audioFileString wa)
                            , style "width" "100px"
                            , (Html.Attributes.type_ "audio/mp4")
                            ]
                            []
                    ]
            
            None ->
                column [ width (fillPortion 1), alignRight, spacing 20 ]
                    [ Input.button
                        [ centerX, Border.width 2, Border.rounded 6, padding 10 ]
                        { onPress = Just GuessedRight, label = buttonLabel wa }
                    ]
                
            Left ->
                column [ width (fillPortion 1), alignRight, spacing 20 ]
                    [ Input.button
                        [ centerX, Border.width 2, Border.rounded 6, padding 10 ]
                        { onPress = Nothing, label = buttonLabel wa }
                    , el [ moveRight 25 ] <| html <|
                        audio
                            [ controls True, src (audioFileString wa)
                            , style "width" "100px"
                            , (Html.Attributes.type_ "audio/mp4")
                            ]
                            []
                    ]


buttonLabel : WordAudio -> Element Msg
buttonLabel wa =
    column []
    [ el [ centerX ] <| text (wordString wa)
    , el [ centerX ] <| text (ipaString wa.wordIpa)
    ]
    
    
ipaGroupPairString : TestSet -> String
ipaGroupPairString { ipaGroupPair } =
    let
        (IpaPair pair) =
            ipaGroupPair
    in
    ipaString (first pair) ++ " vs " ++ ipaString (second pair)


ipaString : String -> String
ipaString s =
    "/" ++ s ++ "/"



wordString : WordAudio -> String
wordString wa =
    case String.split "-" wa.audio of
        [] ->
            "Error reading word"
        
        s::_ ->
            s


audioFileString : WordAudio -> String
audioFileString wa =
    (audioDir ++ wa.wordIpa ++ "/" ++ wa.audio )


color =
    { blue = rgb255 0x72 0x9F 0xCF
    , darkCharcoal = rgb255 0x2E 0x34 0x36
    , lightBlue = rgb255 0xC5 0xE8 0xF7
    , lightGrey = rgb255 0xE0 0xE0 0xE0
    , white = rgb255 0xFF 0xFF 0xFF
    , red = rgb255 0xFF 0x10 0x10
    , green = rgb255 0x20 0xBF 0x55
    }



type IpaPairSet =
    IpaPairSet (Set ( String, String ))

empty : IpaPairSet
empty =
    IpaPairSet Set.empty

add : IpaPair -> IpaPairSet -> IpaPairSet
add (IpaPair pair) (IpaPairSet set) =
    IpaPairSet (Set.insert pair set)

remove : IpaPair -> IpaPairSet -> IpaPairSet
remove (IpaPair pair) (IpaPairSet set) =
    IpaPairSet (Set.remove pair set)

toList : IpaPairSet -> List IpaPair
toList (IpaPairSet set) =
    List.map
        (\pair -> IpaPair pair)
        (Set.toList set)

member : IpaPair -> IpaPairSet -> Bool
member (IpaPair pair) (IpaPairSet set) =
    Set.member pair set

isEmpty : IpaPairSet -> Bool
isEmpty (IpaPairSet set) =
    Set.isEmpty set
