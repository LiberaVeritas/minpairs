# minpairs


An app to practice discriminating between minimal pairs in French

https://liberaveritas.gitlab.io/minpairs/


## Resources

[List of minimal pairs](https://web.archive.org/web/20200804073656/https://minimalpairs.net/en/fr) \
[Wiktionary (IPA and homophones for words)](https://www.wiktionary.org) \
[IPA tongue position charts](https://livefluent.com/french-pronunciation-the-ultimate-guide/)
